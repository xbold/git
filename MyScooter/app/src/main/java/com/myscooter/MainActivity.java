package com.myscooter;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


public class MainActivity extends AppCompatActivity {
    boolean flag = false;

    public boolean checkLocationPermission() {
        String permission = "android.permission.ACCESS_FINE_LOCATION";
        int res = this.checkCallingOrSelfPermission(permission);
        return (res == PackageManager.PERMISSION_GRANTED);
    }

    private String providerId = LocationManager.GPS_PROVIDER;
    private LocationManager mLocationManager;
    private LocationListener mLocationListener;
    double latitudine = 0;
    double longitudine = 0;

    private static final int MIN_DIST = 0;
    private static final int MIN_PERIOD = 5000;
    final int LOCATION_PERMISSION_REQUEST_CODE = 1252;
    Switch luciLed;
    Switch quad;
    Switch acc;
    CountDownTimer count;
    TextView txt;
    Button clacson;
    Button btLogin;
    Button posiz;
    String luciOn = "http://192.168.4.1/ON";
    String luciOff = "http://192.168.4.1/OFF";
    String clacsonOn = "http://192.168.4.1/CLACSON";
    String clacsonOff = "http://192.168.4.1/CLACSONOFF";
    String quadroOn = "http://192.168.4.1/QUADROON";
    String quadroOff = "http://192.168.4.1/QUADROOFF";
    String accensioneOn = "http://192.168.4.1/ACCENSIONEON";
    String accensioneOff = "http://192.168.4.1/ACCENSIONEOFF";
    String up = "http://192.168.4.1/UPDATE";
    CountDownTimer update;
    String[] risp = new String[1];


    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mLocationManager = (LocationManager) this.getSystemService(LOCATION_SERVICE);
        mLocationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                latitudine = location.getLatitude();
                longitudine = location.getLongitude();
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        };
        setContentView(R.layout.activity_main);
        luciLed = findViewById(R.id.switchLed);
        quad = findViewById(R.id.switchQuadro);
        acc = findViewById(R.id.switchAcc);
        clacson = findViewById(R.id.buz);
        btLogin = findViewById(R.id.login);
        txt = findViewById(R.id.editText);
        posiz = findViewById(R.id.posizione);

        //Abilitazione del wifi
        WifiManager wifi = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        wifi.setWifiEnabled(true);
        Toast.makeText(getApplicationContext(), "Wifi On", Toast.LENGTH_LONG).show();
        Update();
        risp[0] = "";

        //Listener dei diversi pulsanti
        luciLed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (luciLed.isChecked()) {
                    sendHttpGet(luciOn);
                } else {
                    sendHttpGet(luciOff);
                }
            }
        });

        quad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (quad.isChecked()) {
                    sendHttpGet(quadroOn);
                } else {
                    sendHttpGet(quadroOff);
                }
            }
        });

        acc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (quad.isChecked()) {
                    if (acc.isChecked()) {
                        sendHttpGet(accensioneOn);
                    } else {
                        sendHttpGet(accensioneOff);
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Accendi il quadro prima!", Toast.LENGTH_LONG).show();
                    acc.setChecked(false);
                }

            }
        });

        posiz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt.setText(latitudine + " " + longitudine);
            }
        });

        clacson.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                if (!flag) {
                    sendHttpGet(clacsonOn);
                    update.cancel();
                    clacsonPremuto(action);
                }

                switch (action) {

                    case MotionEvent.ACTION_UP:
                        count.cancel();
                        sendHttpGet(clacsonOff);
                        flag = false;
                        Update();
                }
                return false;
            }
        });


    }

    protected void onResume() {
        super.onResume();
        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (!mLocationManager.isProviderEnabled(providerId)) {
            Intent gpsOptionIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(gpsOptionIntent);
        } else {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            mLocationManager.requestLocationUpdates(providerId, MIN_PERIOD, MIN_DIST, this.mLocationListener);
        }
    }

    protected void onPause() {
        super.onPause();
        mLocationManager.removeUpdates(this.mLocationListener);
    }

    private void clacsonPremuto(final int action) {
        count = new CountDownTimer(10000, 100) {


            @Override
            public void onTick(long millisUntilFinished) {
                flag = true;
                sendHttpGet(clacsonOn);
            }

            @Override
            public void onFinish() {
                sendHttpGet(clacsonOff);
                flag = false;
            }
        }.start();
    }

    private void sendHttpGet(String url) {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                risp[0] = response.body().string();
            }
        });
    }

    private void Update() {
        update = new CountDownTimer(2000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                sendHttpGet(up);
                String[] str = risp[0].split(";");
                if (str.length > 1) {
                    if (str[1].equals("200")) {
                        quad.setChecked(true);
                    } else if (str[1].equals("201")) {
                        quad.setChecked(false);
                    }
                    if (str[2].equals("300")) {
                        luciLed.setChecked(true);
                    } else if (str[2].equals("301")) {
                        luciLed.setChecked(false);
                    }
                }
            }

            @Override
            public void onFinish() {
                Update();
            }
        }.start();
    }

    /*@Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        if (requestCode == LOCATION_PERMISSION_REQUEST_CODE) {

            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                // start to find location...

            } else { // if permission is not granted

                // decide what you want to do if you don't get permissions
            }
        }
    }*/

    /*private void openActivity(Class act) {
        Intent intent=new Intent(this, act);
        startActivity(intent);
    }*/


}
