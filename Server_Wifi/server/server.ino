#include <ESP8266WiFi.h>
const int LED = 16;
const int Bz = 5;
const int Qua = 4;
const int Acc = 0;
const int Alarm = 14;
String QuadCod = "201";
String QuadroOn = "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n\r\n";
String QuadroOff = "HTTP/1.1 201 OK\r\nContent-Type: text/html\r\n\r\n";
String LedCod = "301";
String LedOn = "HTTP/1.1 300 OK\r\nContent-Type: text/html\r\n\r\n";
String LedOff = "HTTP/1.1 301 OK\r\nContent-Type: text/html\r\n\r\n";
String Clacson = "HTTP/1.1 302 OK\r\nContent-Type: text/html\r\n\r\n";
String ClacsonOff = "HTTP/1.1 303 OK\r\nContent-Type: text/html\r\n\r\n";
String AccensioneOn = "HTTP/1.1 400 OK\r\nContent-Type: text/html\r\n\r\n";
String AccensioneOff = "HTTP/1.1 401 OK\r\nContent-Type: text/html\r\n\r\n";
String Allarme = "HTTP/1.1 700 OK\r\nContent-Type: text/html\r\n\r\n";
unsigned long prec = 0;

WiFiServer server(80); //Initialize the server on Port 80
void setup() {

  WiFi.mode(WIFI_AP); //Our ESP8266-12E is an AccessPoint
  WiFi.softAP("MyScooter", "_progett@"); // Provide the (SSID, password); .
  server.begin(); // Start the HTTP Server

  Serial.begin(115200); //Start communication between the ESP8266-12E and the monitor window
  IPAddress HTTPS_ServerIP = WiFi.softAPIP(); // Obtain the IP of the Server
  //Serial.println(HTTPS_ServerIP);


  //Richiesta dal CLIENT


  pinMode(LED, OUTPUT);
  pinMode(Bz, OUTPUT);
  pinMode(Qua, OUTPUT);
  pinMode(Acc, OUTPUT);
  pinMode(Alarm, INPUT);
  digitalWrite(Acc, HIGH);

  //Segnale Boot
  digitalWrite(LED, LOW);
  digitalWrite(Bz, HIGH);
  digitalWrite(Qua, LOW);
  delay(100);
  digitalWrite(LED, HIGH);
  digitalWrite(Bz, LOW);
  digitalWrite(Qua, HIGH);
}

void loop() {

  WiFiClient client = server.available();
  /*if (digitalRead(Alarm)==1){
    client.flush();
    client.print(Allarme);
    delay(1);
    }*/
  if (!client) {
    unsigned long cur = millis();
    if (cur - prec > 150) {
      prec = cur;
      if (digitalRead(Bz) == 0) {
        digitalWrite(Bz, HIGH);
        //Serial.print("Spento ");
        //Serial.println(millis());
      }
    }
    return;
  }

  String request = client.readStringUntil('\r');
  //Serial.println(request);

  String LimitVoltage = "HTTP/1.1 500 OK\r\nContent-Type: text/html\r\n\r\n";
  //Voltmetro
  int sensorValue = analogRead(A0);
  float voltage = sensorValue / 44.16;

  if (sensorValue == 1024) {
    client.flush();
    client.print(LimitVoltage);
    delay(1);
  }
  //Serial.println(sensorValue);


  String u = "HTTP/1.1 600 OK\r\nContent-Type: text/html\r\n\r\n";
  u += String(voltage, 2) + ";";
  u += QuadCod + ";";
  u += LedCod + ";";

  if (request.indexOf("/OFF") != -1) {
    digitalWrite(LED, HIGH);
    client.flush();
    client.print(LedOff);
    LedCod = "301";
    delay(1);
  }
  else if (request.indexOf("/ON") != -1) {
    digitalWrite(LED, LOW);
    client.flush();
    client.print(LedOn);
    LedCod = "300";
    delay(1);
  }
  else if (request.indexOf("/UPDATE") != -1) {
    client.flush();
    client.print(u);
    delay(1);
  }
  else if (request.indexOf("/CLACSONOFF") != -1) {
    digitalWrite(Bz, HIGH);
    client.flush();
    client.print(ClacsonOff);
    delay(1);
  }
  else if (request.indexOf("/CLACSON") != -1) {
    digitalWrite(Bz, LOW);
    prec = millis();
    client.flush();
    client.print(Clacson);
    delay(1);
  }
  else if (request.indexOf("/QUADROON") != -1) {
    digitalWrite(Qua, LOW);
    client.flush();
    client.print(QuadroOn);
    QuadCod = "200";
    delay(1);
  }
  else if (request.indexOf("/QUADROOFF") != -1) {
    digitalWrite(Qua, HIGH);
    client.flush();
    client.print(QuadroOff);
    QuadCod = "201";
    delay(1);
  }
  else if (request.indexOf("/ACCENSIONEON") != -1) {
    digitalWrite(Acc, LOW);
    delay(750);
    digitalWrite(Acc, HIGH);
    client.flush();
    client.print(AccensioneOn);
    delay(1);
  }
  else if (request.indexOf("/ACCENSIONEOFF") != -1) {
    digitalWrite(Acc, HIGH);
    client.flush();
    client.print(AccensioneOff);
    delay(1);
  }
}
